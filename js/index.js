$(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $('.carousel').carousel({
          interval: 4000
        });
        $('#contacto').on('show.bs.modal',function(e){
          console.log('el modal se esta mostrando');          
          $('.btn-contacto').removeClass('btn-contacto').addClass('btn-reserva');
          $('.btn-contacto').prop('disabled', false);
        });
        $('#contacto').on('shown.bs.modal',function(e){
          console.log('el modal se mostró');
        });
        $('#contacto').on('hide.bs.modal',function(e){
          console.log('el modal se oculta');
        });
        $('#contacto').on('hidden.bs.modal',function(e){
          console.log('el modal se ocultó');
          $('.btn-contacto').prop('disabled', false);
          $('.btn-reserva').removeClass('btn-reserva').addClass('btn-contacto');      
        });
      });